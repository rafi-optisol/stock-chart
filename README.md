# Intern + Angular

This is a test project to reproduce the duplicate bullets issues in amChart using Angular v12 and amchart v5.3.9. 

## Get started

### Clone the repo

```shell
git clone https://github.com/bryanforbes/intern-angular
cd intern-angular
```

### Install npm packages

Install the `npm` packages described in the `package.json` and verify that it works:

```shell
npm install
ng serve
```

Shut it down manually with `Ctrl-C`.


