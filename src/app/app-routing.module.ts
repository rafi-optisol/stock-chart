import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StockChartCanvasComponent } from './components/stock-chart/stock-chart-canvas/stock-chart-canvas.component';
import { StockChartGridComponent } from './components/stock-chart-grid/stock-chart-grid.component';

const routes: Routes = [
  { path: '', redirectTo: '/stock-chart-grid', pathMatch: 'full'},

  {
    path: 'stock-chart-grid', component: StockChartGridComponent
  },
  {
    path: 'stock-chart-canvas', component: StockChartCanvasComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
