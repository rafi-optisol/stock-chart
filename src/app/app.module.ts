import { NgModule,CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from 'src/environments/environment';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ToastrModule } from 'ngx-toastr';
import { MaterialModule } from './components/material.module';
import { ServiceModule } from './services/service.module';

import { NgxPaginationModule } from 'ngx-pagination';
import { NgxLoadingModule } from 'ngx-loading';

import { StockChartComponent } from './components/stock-chart/stock-chart.component';
import { StockChartControlComponent } from './components/stock-chart/stock-chart-control/stock-chart-control.component';
import { StockChartCanvasComponent } from './components/stock-chart/stock-chart-canvas/stock-chart-canvas.component';
import { StockChartGridComponent } from './components/stock-chart-grid/stock-chart-grid.component';
@NgModule({
  declarations: [
    AppComponent,
    StockChartGridComponent,
    StockChartComponent,
    StockChartControlComponent,
    StockChartCanvasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
 
    FormsModule,

    NgxLoadingModule.forRoot({}),
    TabsModule.forRoot(),
    TranslateModule.forRoot({
      defaultLanguage: 'en',
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    }),
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-top-right',
      enableHtml: false,
      closeButton: true,
      tapToDismiss: true,
    }),
    ServiceModule,
  ],
  providers: [],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}
