import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import icInstrument from '@iconify/icons-ic/twotone-business';
import icChart from '@iconify/icons-ic/twotone-show-chart';


@Component({
    selector: 'stock-chart',
    templateUrl: './stock-chart.html',
    styleUrls: ['./stock-chart.scss']
})
export class StockChartComponent implements OnInit {
    icInstrument = icInstrument;
    icChart = icChart;
    header: string = "Amazon Com Inc Com";
    addCompareData: any;
    updateTimeSeriesData: string;
    getSeriesColorCode: any = {};
    showingTransactions: boolean;
    opened: boolean;
    mode: any = 'side';
    @Input() id: any;
    @Input() selectedLayout: any;
    @Input() searchData: any;
    getfilterdata: any;
    valid: any;

    constructor(private cdRef: ChangeDetectorRef) { }

    ngOnInit(): void {
        // this.quickcomp.searchableData();
    }
    async ngOnChanges() {
        console.log(this.searchData )
          if(this.searchData == 'amazon'){
            this.getfilterdata = [
                {
                    "hid": 111617,
                    "name": "Amazon Com Inc Com",
                    "type": "CS",
                    "exchange": "NMQ",
                    "ticker": "AMZN",
                    "isin": "US0231351067",
                    "cusip": "023135106",
                    "sedol": "2000019",
                    "ric": "AMZN.O",
                    "quicksearch_fields": [
                        "hid",
                        "ric",
                        "name",
                        "ticker",
                        "isin",
                        "cusip",
                        "sedol"
                    ],
                    "country_code": "US",
                    "cached_search_text": "◬111617◬AMZN.O◬Amazon Com Inc Com◬AMZN◬US0231351067◬023135106◬2000019"
                }
            ]
          }
          if(this.searchData == 'TSL.AX'){
            this.getfilterdata = [
                {
                    "hid": 272740,
                    "name": "TITANIUM SANDS LTD NPV",
                    "type": "CS",
                    "exchange": "AAS",
                    "ticker": "TSL",
                    "isin": "AU000000TSL2",
                    "cusip": null,
                    "sedol": "BDFC8H4   ",
                    "ric": "TSL.AX",
                    "quicksearch_fields": [
                        "hid",
                        "ric",
                        "name",
                        "ticker",
                        "isin",
                        "cusip",
                        "sedol"
                    ],
                    "country_code": "AU",
                    "cached_search_text": "◬272740◬TSL.AX◬TITANIUM SANDS LTD NPV◬TSL◬AU000000TSL2◬null◬BDFC8H4   "
                }
            ]
          }

    }
    ngAfterViewInit() {
        this.cdRef.detectChanges();
    }


    addTimeSeries(newSeries:any): void {
        this.addCompareData = newSeries;
    }

    updateTimeSeries(value:any): void {
        this.updateTimeSeriesData = value;
    }

    getSeriesColor(color:any): void {
        this.getSeriesColorCode = color;
    }

    isTransactionsEnabled(enabled: boolean) {
        this.showingTransactions = enabled;
    }

    isSideNavOpened(sideNavConfig: any) {
        if (!sideNavConfig.Open) {
            this.opened = sideNavConfig.Opened;
            this.mode = sideNavConfig.Mode;
        }
        else {
            this.opened = sideNavConfig.Opened;
            this.mode = sideNavConfig.Mode;
        }
    }
}
