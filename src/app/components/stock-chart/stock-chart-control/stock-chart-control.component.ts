import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, HostListener, Input, OnChanges, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

import icAdd from '@iconify/icons-ic/twotone-add-box';
import icClose from '@iconify/icons-ic/twotone-close';
import icMaximize from '@iconify/icons-ic/twotone-fullscreen';
import icMinimize from '@iconify/icons-ic/twotone-fullscreen-exit';
import icMenu from '@iconify/icons-ic/twotone-more-vert';
import icTransactions from '@iconify/icons-ic/twotone-room';

@Component({
    selector: 'stock-chart-control',
    templateUrl: './stock-chart-control.html',
    styleUrls: ['./stock-chart-control.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class StockChartControlComponent implements OnInit, OnChanges {
    chartPresets: { name: string, series: any[] }[];
    presetsForm = new FormControl();
    initialized: any =false;
    SearchRecord: any;
    @Input()   set getSearchRecord(data: string) {
            if(data){
            setTimeout(() => {
            this.SearchRecord = data
            this.dataafterviewinit()
            },0)
        }
    }
    @Input() updateTimeSeriesData: string;
    @Input() getSeriesColorCode: any = {};
    @Output() addTimeSeries = new EventEmitter<string>();
    @Output() isTransactionsEnabled = new EventEmitter<boolean>();
    @Output() isSideNavOpened = new EventEmitter<boolean>();

    icExcel ={
        "body": "<path d=\"M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm60.1 106.5L224 336l60.1 93.5c5.1 8-.6 18.5-10.1 18.5h-34.9c-4.4 0-8.5-2.4-10.6-6.3C208.9 405.5 192 373 192 373c-6.4 14.8-10 20-36.6 68.8c-2.1 3.9-6.1 6.3-10.5 6.3H110c-9.5 0-15.2-10.5-10.1-18.5l60.3-93.5l-60.3-93.5c-5.2-8 .6-18.5 10.1-18.5h34.8c4.4 0 8.5 2.4 10.6 6.3c26.1 48.8 20 33.6 36.6 68.5c0 0 6.1-11.7 36.6-68.5c2.1-3.9 6.2-6.3 10.6-6.3H274c9.5-.1 15.2 10.4 10.1 18.4zM384 121.9v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z\" fill=\"currentColor\"/>",
        "width": 384,
        "height": 512
    }
    icAdd = {
        "body": "<path fill=\"currentColor\" d=\"M5 19h14V5H5v14zm2-8h4V7h2v4h4v2h-4v4h-2v-4H7v-2z\" opacity=\".3\"/><path fill=\"currentColor\" d=\"M19 3H5a2 2 0 0 0-2 2v14a2 2 0 0 0 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 16H5V5h14v14zm-8-2h2v-4h4v-2h-4V7h-2v4H7v2h4z\"/>",
        "width": 24,
        "height": 24
    };
    icClose = {
        "body": "<path fill=\"currentColor\" d=\"M19 6.41L17.59 5L12 10.59L6.41 5L5 6.41L10.59 12L5 17.59L6.41 19L12 13.41L17.59 19L19 17.59L13.41 12L19 6.41z\"/>",
        "width": 24,
        "height": 24
    };
    icMenu = {
        "body": "<path fill=\"currentColor\" d=\"M12 8c1.1 0 2-.9 2-2s-.9-2-2-2s-2 .9-2 2s.9 2 2 2zm0 2c-1.1 0-2 .9-2 2s.9 2 2 2s2-.9 2-2s-.9-2-2-2zm0 6c-1.1 0-2 .9-2 2s.9 2 2 2s2-.9 2-2s-.9-2-2-2z\"/>",
        "width": 24,
        "height": 24
    };
    icMaximize = {
        "body": "<path fill=\"currentColor\" d=\"M7 14H5v5h5v-2H7v-3zm-2-4h2V7h3V5H5v5zm12 7h-3v2h5v-5h-2v3zM14 5v2h3v3h2V5h-5z\"/>",
        "width": 24,
        "height": 24
    };
    icMinimize = {
        "body": "<path fill=\"currentColor\" d=\"M5 16h3v3h2v-5H5v2zm3-8H5v2h5V5H8v3zm6 11h2v-3h3v-2h-5v5zm2-11V5h-2v5h5V8h-3z\"/>",
        "width": 24,
        "height": 24
    };
    icTransactions = {
        "body": "<path fill=\"currentColor\" d=\"M12 4C9.24 4 7 6.24 7 9c0 2.85 2.92 7.21 5 9.88c2.11-2.69 5-7 5-9.88c0-2.76-2.24-5-5-5zm0 7.5a2.5 2.5 0 0 1 0-5a2.5 2.5 0 0 1 0 5z\" opacity=\".3\"/><path fill=\"currentColor\" d=\"M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zM7 9c0-2.76 2.24-5 5-5s5 2.24 5 5c0 2.88-2.88 7.19-5 9.88C9.92 16.21 7 11.85 7 9z\"/><circle cx=\"12\" cy=\"9\" r=\"2.5\" fill=\"currentColor\"/>",
        "width": 24,
        "height": 24
    };
    showingTransactions: any = false;
    showFiller = false;
    smallScreen: any;
    mode: any;
    opened: boolean;

    @Input() openSideNav: boolean;
    @Input() sidenav: any;
    @Input() selectedLayout: any;
    existingTimeSeries:any;

    @HostListener('window:resize', ['$event'])
    onResize(event:any) {
        this.configureSideNav()
    }

    constructor(private dialog: MatDialog, private snackbar: MatSnackBar, private cdr: ChangeDetectorRef) { }

    ngOnInit(): void {
        this.configureSideNav();
    }

    ngOnChanges() {
        if (this.updateTimeSeriesData) {
            let index = this.existingTimeSeries.findIndex((e:any) => e.factor == this.updateTimeSeriesData);
            if (index >= 0) {
                this.existingTimeSeries.splice(index, 1);
            }
            this.updateTimeSeriesData = "";
        }
        if (this.getSeriesColorCode) {
            let index = this.existingTimeSeries?.length?this.existingTimeSeries.findIndex((e:any) => e.factor == this.getSeriesColorCode.series):-1;
            if (index >= 0) {
                this.existingTimeSeries[index].color = this.getSeriesColorCode.color;
            }
            this.getSeriesColorCode = {};
        }
        if (this.openSideNav) {
            this.mode = 'over';
            this.opened = true;
        }
    }

    dataafterviewinit(){
        if(this.SearchRecord && this.SearchRecord?.length && this.SearchRecord?.length>0) {
            this.existingTimeSeries = [{
                ric: this.SearchRecord[0].hid,
                ricVs: null,
                portfolio: null,
                factor: this.SearchRecord[0].display_name,
                factorVs: '',
                vsOperator: '',
                winsorize_lower: '',
                winsorize_upper: '',
                yAxis: 'right',
                color: null,
                type: 'line',
                useLogScale: false,
                name: this.SearchRecord[0].display_name
            },{
                ric: null,
                ricVs: null,
                portfolio: null,
                factor: "Volume",
                factorVs: '',
                vsOperator: '',
                winsorize_lower: '',
                winsorize_upper: '',
                yAxis: 'right',
                color: null,
                type: 'line',
                useLogScale: false,
                name: "Volume"
            }];
        }
    }
    
    ngAfterViewInit() {
        this.dataafterviewinit()
    }
    
    configureSideNav() {
        let sideNavConfig:any;
        this.smallScreen = window.innerWidth < 960 ? true : false;
        if (!this.smallScreen && this.selectedLayout != 'sides' && this.selectedLayout != 'grid') {
            this.mode = "side";
            this.opened = true;
            sideNavConfig = {
                "Mode": this.mode,
                "Opened": this.opened
            }

        } else {
            this.mode = 'over';
            this.opened = false;
            sideNavConfig = {
                "Mode": this.mode,
                "Opened": this.opened
            }
        }
        this.isSideNavOpened.emit(sideNavConfig);
    }

    export2excel() {
    }

    // openTimeSeries(action:any, obj:any) {
    //     obj.action = action;
    //     const dialogRef = this.dialog.open(StockChartFormComponent, {
    //         disableClose: true,
    //         hasBackdrop: true,
    //         backdropClass: '',
    //         width: '350px',
    //         height: '350px',
    //         position: { right: '50%', left: '50%' },
    //         data: obj
    //     });
    //     dialogRef.afterClosed().subscribe(result => {
    //         if (result.event == 'Add') {
    //             if (this.existingTimeSeries.length > 0) {
    //                 let checkSeries = this.existingTimeSeries.filter((e:any) => e.factor == result.data.factor)
    //                 if (checkSeries.length > 0) {
    //                     this.snackbar.open("Series already added", 'ERROR', { duration: 10 * 1000, panelClass: 'error' });
    //                 }
    //                 else {
    //                     this.existingTimeSeries.push(result.data);
    //                     this.addTimeSeries.emit(result);
    //                 }
    //             }
    //             else {
    //                 this.existingTimeSeries.push(result.data);
    //                 this.addTimeSeries.emit(result);
    //             }
    //         }
    //         else if (result.event == 'Update') {
    //             for (var i = 0; i <= this.existingTimeSeries.length - 1; i++) {
    //                 if (this.existingTimeSeries[i]['factor'] == result.data.factor) {
    //                     this.existingTimeSeries[i] = result.data;
    //                 }
    //             }
    //             this.addTimeSeries.emit(result);
    //         }
    //         else if (result.event == 'Delete') {
    //             if (result.data.factor != this.SearchRecord[0].display_name && result.data.factor != "Volume") {
    //                 let index = this.existingTimeSeries.findIndex((e:any) => e.factor == result.data.factor);
    //                 if (index >= 0) {
    //                     this.existingTimeSeries.splice(index, 1);
    //                 }
    //             }
    //             else if(result.data.factor == this.SearchRecord[0].display_name) {
    //                 this.snackbar.open("The anchor/first TS cannot be deleted, so we have hidden it for you", 'Warning', { duration: 10 * 1000, panelClass: 'warning' });
    //             }
    //             else if(result.data.factor == "Volume") {
    //                 this.snackbar.open("The volume series cannot be deleted, so we have hidden it for you", 'Warning', { duration: 10 * 1000, panelClass: 'warning' });
    //             }
    //             this.addTimeSeries.emit(result);
    //         }
    //     });
    // }

    showTransactions(value:any) {
        this.showingTransactions = value;
        this.isTransactionsEnabled.emit(this.showingTransactions);
    }
}
