import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockChartControlComponent } from './stock-chart-control.component';

describe('StockChartControlComponent', () => {
    let component: StockChartControlComponent;
    let fixture: ComponentFixture<StockChartControlComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [StockChartControlComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(StockChartControlComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
