import * as am5 from "@amcharts/amcharts5";
import * as am5stock from "@amcharts/amcharts5/stock";
import am5themes_Animated from "@amcharts/amcharts5/themes/Animated";
import am5themes_Responsive from '@amcharts/amcharts5/themes/Responsive';
import * as am5xy from "@amcharts/amcharts5/xy";
import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { DateTime } from 'luxon';
import icMenu from '@iconify/icons-ic/twotone-menu';
import { Subject } from "rxjs";


@Component({
    selector: 'stock-chart-canvas',
    templateUrl: './stock-chart-canvas.html',
    styleUrls: ['./stock-chart-canvas.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class StockChartCanvasComponent implements OnInit, OnChanges {
    @Input() newSeries: string;
    stockChart: any;
    currentGranularity: any;
    valueAxis: any;
    dateAxis: any;
    root: any;
    mainPanel: any;
    valueSeries: any;
    transactionSeries: any;
    valueLegend: any;
    volumeAxisRenderer: any;
    toolbar: any;
    volumeValueAxis: any;
    volumeSeries: any;
    comparingSeries: any;
    targetSeries: any;
    container: any;
    initialized: boolean =false;
    @Input() addCompareData: any;
    @Input() showingTransactions: boolean;
    SearchRecord: any;
    getrecord: string;
    @Input()
    set getSearchRecord(data: string) {
       if(data){
           this.getrecord = data
           setTimeout(() => {
                if(this.initialized == true) {
                    this.SearchRecord=this.getrecord; 
                  console.log(this.SearchRecord)

                    this.dataAfterViewInit();
                }
           }, 0);
        }
    }
    @Output() updateTimeSeries = new EventEmitter<string>();
    @Output() getSeriesColorCode = new EventEmitter<any>();
    transactions: any = [];
    seriesData: any = [];
    yAxis: any;
    @Input() sidenav: any;
    @Input() opened: any;
    @Input() id: any ;
    @Input() selectedLayout: any;
    indicator: any;
    hourglass: any;
    hourglassanimation: any;
    icMenu = {
        "body": "<path fill=\"currentColor\" d=\"M3 18h18v-2H3v2zm0-5h18v-2H3v2zm0-7v2h18V6H3z\"/>",
        "width": 24,
        "height": 24
    };
    existingSeries: any;
    onChanges = new Subject<SimpleChanges>();
    constructor() {
    }

    ngOnInit(): void {
 
    }

    ngOnChanges() {
        console.log(this.id)
        this.showTransactions(this.showingTransactions);
        if (this.addCompareData && this.addCompareData.event == "Delete") {
            if (this.addCompareData.data.factor != this.SearchRecord[0].display_name && this.addCompareData.data.factor != "Volume") {
                let index = this.existingSeries.findIndex((e: { factor: any; }) => e.factor == this.addCompareData.data.factor);
                let currentSeries = this.stockChart.get("compareSeries" + this.addCompareData.data.factor);
                this.valueLegend.data.removeValue(currentSeries);
                this.mainPanel.series.removeValue(currentSeries);
                this.existingSeries.splice(index, 1);
            }
            else {
                if (this.addCompareData.data.factor == this.SearchRecord[0].display_name) {
                    this.valueSeries.hide();
                }
                if (this.addCompareData.data.factor == "Volume") {
                    this.volumeSeries.hide();
                }
            }
        }
        else {
            if (this.existingSeries?.length&&this.existingSeries.length >= 1 && this.addCompareData) {
                let checkSeries = this.existingSeries.filter((e: { factor: any; }) => e.factor == this.addCompareData.data.factor);
                if (checkSeries.length >= 1) {
                    if (this.addCompareData.data.factor == this.SearchRecord[0].display_name) {
                        let isHidden = this.valueSeries.isHidden();
                        if (isHidden)
                            this.valueSeries.show();
                    }
                    else if (this.addCompareData.data.factor == 'Volume') {
                        let isHidden = this.volumeSeries.isHidden();
                        if (isHidden)
                            this.volumeSeries.show();
                    }
                }
                else {
                    if (this.addCompareData.event == "Update") {
                        for (var i = 0; i <= this.existingSeries.length - 1; i++) {
                            if (this.existingSeries[i]['factor'] == this.addCompareData.data.factor) {
                                this.existingSeries[i] = this.addCompareData.data;
                            }
                        }
                    }
                    else {
                        this.existingSeries.push(this.addCompareData.data);
                        this.addComparingSeries(this.addCompareData);
                    }
                }
            }
            else if (this.existingSeries?.length&&this.existingSeries.length < 1) {
                this.existingSeries.push(this.addCompareData.data);
            }
        }
    }

    dataAfterViewInit() {
        if (this.SearchRecord && this.SearchRecord?.length && this.SearchRecord?.length > 0) {
            this.existingSeries = [{
                ric: this.SearchRecord[0].hid,
                ricVs: null,
                portfolio: null,
                factor: this.SearchRecord[0].display_name,
                factorVs: '',
                vsOperator: '',
                winsorize_lower: '',
                winsorize_upper: '',
                yAxis: 'right',
                color: null,
                type: 'line',
                useLogScale: false,
                name: this.SearchRecord[0].display_name
            },
            {
                ric: null,
                ricVs: null,
                portfolio: null,
                factor: "Volume",
                factorVs: '',
                vsOperator: '',
                winsorize_lower: '',
                winsorize_upper: '',
                yAxis: 'right',
                color: null,
                type: 'line',
                useLogScale: false,
                name: "Volume"
            }];
            this.configureChart();
        }
    }

    ngAfterViewInit() {
        this.initialized = true;
    }

    async maybeDisposeRoot(divId:any) {
        am5.array.each(am5.registry.rootElements, function (root) {
            if (root.dom?.id == divId) {
                root.dispose();
            }
        });
    }

    private  configureChart() {
         this.maybeDisposeRoot('chartdiv'+ this.id);
         this.root = am5.Root.new('chartdiv' + this.id);

        // Set themes
        this.root.setThemes([
            am5themes_Animated.new(this.root),
            am5themes_Responsive.new(this.root)
        ]);

        // Create a stock chart
        this.stockChart = this.root.container.children.push(am5stock.StockChart.new(this.root, {
            autoSetPercentScale: false,
        }));


        // Set global number format
        this.root.numberFormatter.set("numberFormat", "#,###.00");

        // Create a main stock panel (chart)
        this.mainPanel = this.stockChart.panels.push(am5stock.StockPanel.new(this.root, {
            wheelY: "zoomX",
            panX: true,
            panY: true,
            height: am5.percent(100)
        }));

        // Create value axis
        this.valueAxis = this.mainPanel.yAxes.push(am5xy.ValueAxis.new(this.root, {
            renderer: am5xy.AxisRendererY.new(this.root, {}),
            x: am5.percent(35)
        }));

        this.dateAxis = this.mainPanel.xAxes.push(am5xy.GaplessDateAxis.new(this.root, {
            baseInterval: {
                timeUnit: "day",
                count: 1
            },
            groupData: true,
            groupCount: 600,
            minGridDistance: 50,
            renderer: am5xy.AxisRendererX.new(this.root, {}),
            tooltip: am5.Tooltip.new(this.root, {}),
        }));

        //Add x axis range inorder to add one more month from current date
        var range = this.dateAxis.createAxisRange(this.dateAxis.makeDataItem({
            value: DateTime.local().minus({ days: 1 }).toJSDate(),
            endValue: DateTime.local().plus({ months: 1 }).toJSDate()
        }))

        //Fill range
        var axisFill = range.get("axisFill");
        if (axisFill) {
            axisFill.setAll({
                fill: am5.color(0x396478),
                fillOpacity: 0.5,
            });
        }

        //Add x axis range inorder to add end of time series text
        var range2 = this.dateAxis.createAxisRange(this.dateAxis.makeDataItem({
            value: DateTime.local().minus({ days: 1 }).toJSDate()
        }));



        //Customize grid and label
        var grid = range2.get("grid");
        if (grid) {
            grid.setAll({
                visible: true,
                stroke: am5.color(0x396478),
                strokeOpacity: 0.9,
                strokeDasharray: [5, 2]
            })
        }

        var axisFill = range2.get("axisFill");
        if (axisFill) {
            axisFill.setAll({
                visible: true,
                fill: am5.color(0x396478),
                fillOpacity:0.5
            })
        }

        var label = range2.get("label");
        if (label) {
            label.setAll({
                text: "End of Time Series",
                fontSize: 14,
                inside: true,
                dy: -50,
                dx: 15,
                rotation: 90,
                scale: 0.7,
                paddingTop: 0,
                paddingBottom: 0,
                paddingLeft: 0,
            })
        }

        // Add transaction series
        this.transactionSeries = this.mainPanel.series.push(am5xy.LineSeries.new(this.root, {
            name: "Transaction",
            valueXField: "adate",
            valueYField: "Transaction",
            calculateAggregates: true,
            focusable: true,
            xAxis: this.dateAxis,
            yAxis: this.valueAxis,
            tooltip: am5.Tooltip.new(this.root, { })
        }));

        this.transactionSeries.bullets.push((root: am5.Root, series: any, dataItem: { dataContext: { transaction_data: { type: string; }; }; }) => {
            if (dataItem.dataContext.transaction_data.type == "Buy" || dataItem.dataContext.transaction_data.type == "Buy Cover"){                
                return am5.Bullet.new(root, {
                    sprite: am5.Circle.new(root, {
                        radius: 6,
                        fill: am5.color(0x39FF74),
                        stroke: am5.color(0xffffff),
                        strokeWidth: 0
                    })
                });
            }
            else {
                return am5.Bullet.new(root, {
                    sprite: am5.Circle.new(root, {
                        radius: 6,
                        fill: am5.color(0xFF7439),
                        stroke: am5.color(0xffffff),
                        strokeWidth: 0
                    })
                });
            }        
        });

        this.transactionSeries.strokes.template.setAll({
            strokeWidth: 1
        });

        var tooltip:any = am5.Tooltip.new(this.root, {
            getFillFromSprite: false,
            getStrokeFromSprite: false,
            autoTextColor: false,
            getLabelFillFromSprite: false,
            labelText: "default text"
        });
        // set tooltip text 
        tooltip.adapters.add("labelText", function<T extends object>(text:any, target:any ) {
            // target.dataItem will hold current data item
            if(target && target.dataItem && target.dataItem.dataContext && target.dataItem.dataContext.transaction_data){
                var type = target.dataItem.dataContext.transaction_data.type;
                var shares = Math.abs(target.dataItem.dataContext.transaction_data.shares);
                var price_per_share = target.dataItem.dataContext.transaction_data.price_per_share;
                var fund_name = target.dataItem.dataContext.transaction_data.fund_name;
                var multistrategy_name = target.dataItem.dataContext.transaction_data.multistrategy_name? '.'+target.dataItem.dataContext.transaction_data.multistrategy_name : '';
                //{transaction_data.type} {Math.abs(transaction_data.shares)} @ {transaction_data.price_per_share} - {transaction_data.fund_name} .{transaction.multistrategy_name}"
                return type +' '+shares+' @ '+price_per_share+' '+fund_name +' '+multistrategy_name;
            } else {
                return text; // returns default label text
            }
            
        });
        // set background collor 
        tooltip.get("background").adapters.add("fill", function<T extends object>(fill:any, target:any) {
            if(target && target.dataItem && target.dataItem.dataContext && target.dataItem.dataContext.transaction_data && (target.dataItem.dataContext.transaction_data.type =='Buy' || target.dataItem.dataContext.transaction_data.type =='Buy Cover')){
                return am5.color(0x39FF74) ; 
            }
            else {
                return am5.color(0xFF7439);
            }  
        });

        this.transactionSeries.set("tooltip", tooltip);

        // hides line for transaction series
        // this.transactionSeries.strokes.template.set("forceHidden", true);
        
        // Set main value series
        this.stockChart.set("stockSeries", this.transactionSeries);
        // End transaction series

        // Add value series
        this.valueSeries = this.mainPanel.series.push(am5xy.LineSeries.new(this.root, {
            name: this.SearchRecord[0]?.display_name,
            valueXField: "adate",
            valueYField: "aclose",
            calculateAggregates: true,
            focusable: true,
            xAxis: this.dateAxis,
            yAxis: this.valueAxis,
            legendValueText: "Close: [bold]{valueY}[/]",
            tooltip: am5.Tooltip.new(this.root, {
                keepTargetHover: true,
                pointerOrientation: "left",
                labelText: "{valueY.formatNumber('#.00')}"
            })
        }));
        // Set main value series color
        this.valueSeries.set("fill", am5.color(0xc6b54e));
        this.valueSeries.set("stroke", am5.color(0xc6b54e));
        this.valueSeries.fills.template.set(
            "fillGradient",
            am5.LinearGradient.new(this.root, {
                stops: [
                    {
                        opacity: 1,
                        offsets: 0
                    },
                    {
                        opacity: 0,
                        offsets: 1
                    }
                ],
                rotation: 90
            })
        );

        this.valueSeries.fills.template.setAll({
            visible: true,
            fillOpacity: 0.35
        });

        this.valueSeries.strokes.template.setAll({
            strokeWidth: 2
        });

        // Set main value series
        this.stockChart.set("stockSeries", this.valueSeries);
        // End value series

        // Add a stock legend
        this.valueLegend = this.mainPanel.plotContainer.children.push(am5stock.StockLegend.new(this.root, {
            stockChart: this.stockChart
        }));

        this.valueLegend.closeButtons.template.events.on("click", (ev: any) => {
            let removeLegend = ev.target.dataItem.dataContext._settings.name;
            let index = this.existingSeries.findIndex((e: { factor: any; }) => e.factor == this.addCompareData.data.factor);
            this.existingSeries.splice(index, 1);
            this.updateTimeSeries.emit(removeLegend);
        });

        this.valueLegend.settingsButtons.template.events.on("click", (ev: any) => {
            this.targetSeries = ev.target.dataItem.dataContext._settings.name;
        });

        this.stockChart.getPrivate("settingsModal").events.on("done", (ev: any) => {
            if (ev.settings === null) {
                // Cancelled
            }
            else {
                // Saved
                this.getSeriesColor(this.targetSeries);
            }
        });

        // Create volume axis
        this.volumeAxisRenderer = am5xy.AxisRendererY.new(this.root, {
            inside: true
        });

        this.volumeAxisRenderer.labels.template.set("forceHidden", true);
        this.volumeAxisRenderer.grid.template.set("forceHidden", true);

        this.volumeValueAxis = this.mainPanel.yAxes.push(am5xy.ValueAxis.new(this.root, {
            numberFormat: "#.#a",
            height: am5.percent(20),
            y: am5.percent(100),
            centerY: am5.percent(100),
            renderer: this.volumeAxisRenderer
        }));

        // Add series
        this.volumeSeries = this.mainPanel.series.push(am5xy.ColumnSeries.new(this.root, {
            name: "Volume",
            clustered: false,
            valueXField: "adate",
            valueYField: "volume",
            xAxis: this.dateAxis,
            yAxis: this.volumeValueAxis,
            legendValueText: "[bold]{valueY.formatNumber('#,###.0a')}[/]"
        }));

        this.volumeSeries.columns.template.setAll({
            strokeOpacity: 0,
            fillOpacity: 0.5
        });

        // color columns by stock rules
        this.volumeSeries.columns.template.adapters.add("fill", (fill: any, target: any) => {
            let dataItem = target.dataItem;
            if (dataItem) {
                return this.stockChart.getVolumeColor(dataItem);
            }
            return fill;
        })

        // Set main series
        this.stockChart.set("volumeSeries", this.volumeSeries);
        this.valueLegend.data.setAll([this.valueSeries, this.volumeSeries]);
        // this.transactionSeries.hide();
        // Add cursor(s)
        this.mainPanel.set("cursor", am5xy.XYCursor.new(this.root, {
            behavior: "zoomY",
            yAxis: this.valueAxis,
            xAxis: this.dateAxis,
            snapToSeries: [this.valueSeries],
            snapToSeriesBy: "y!"
        }));

        // Add scrollbar
        let scrollbar = this.mainPanel.set("scrollbarX", am5xy.XYChartScrollbar.new(this.root, {
            orientation: "horizontal",
            height: 50
        }));
        this.stockChart.toolsContainer.children.push(scrollbar);

        let sbDateAxis = scrollbar.chart.xAxes.push(am5xy.GaplessDateAxis.new(this.root, {
            baseInterval: {
                timeUnit: "day",
                count: 1
            },
            renderer: am5xy.AxisRendererX.new(this.root, {})
        }));

        let sbValueAxis = scrollbar.chart.yAxes.push(am5xy.ValueAxis.new(this.root, {
            renderer: am5xy.AxisRendererY.new(this.root, {})
        }));

        let sbSeries = scrollbar.chart.series.push(am5xy.LineSeries.new(this.root, {
            valueYField: "aclose",
            valueXField: "adate",
            xAxis: sbDateAxis,
            yAxis: sbValueAxis
        }));

        sbSeries.fills.template.setAll({
            visible: true,
            fillOpacity: 0.3
        });

        // Create a loading indicator
        this.indicator = this.root.container.children.push(am5.Container.new(this.root, {
            width: am5.p100,
            height: am5.p100,
            layer: 1000,
            background: am5.Rectangle.new(this.root, {
                fill: am5.color(0xffffff),
                fillOpacity: 1
            })
        }));

        this.indicator.children.push(am5.Label.new(this.root, {
            text: "Loading...",
            fontSize: 25,
            x: am5.p50,
            y: am5.p50,
            centerX: am5.p50,
            centerY: am5.p50
        }));

        this.hourglass = this.indicator.children.push(am5.Graphics.new(this.root, {
            width: 32,
            height: 32,
            fill: am5.color(0x000000),
            x: am5.p50,
            y: am5.p50,
            centerX: am5.p50,
            centerY: am5.p50,
            dy: -45,
            svgPath: "M12 5v10l9 9-9 9v10h24V33l-9-9 9-9V5H12zm20 29v5H16v-5l8-8 8 8zm-8-12-8-8V9h16v5l-8 8z"
        }));

        this.hourglassanimation = this.hourglass.animate({
            key: "rotation",
            to: 360,
            loops: Infinity,
            duration: 2000,
            easing: am5.ease.inOut(am5.ease.cubic)
        });

        // Load initial data for the first series
        this.currentGranularity = "day";
        this.loadData(this.SearchRecord[0]?.display_name, [this.valueSeries, this.volumeSeries, this.transactionSeries, sbSeries], this.currentGranularity);

        // Set up series type switcher
        let seriesSwitcher = am5stock.SeriesTypeControl.new(this.root, {
            stockChart: this.stockChart,
            currentItem: 'line',
        });

        seriesSwitcher.events.on("selected", (ev: any) => {
            this.setSeriesType(ev.item.id);
        });

        // Stock toolbar
        this.toolbar = am5stock.StockToolbar.new(this.root, {
            container: document.getElementById('chartControls' + this.id),
            stockChart: this.stockChart,
            controls: [
                am5stock.IndicatorControl.new(this.root, {
                    stockChart: this.stockChart,
                    legend: this.valueLegend
                }),
                am5stock.DateRangeSelector.new(this.root, {
                    stockChart: this.stockChart
                }),
                am5stock.PeriodSelector.new(this.root, {
                    stockChart: this.stockChart,
                    periods: [
                        { timeUnit: "month", count: 1, name: "3M" },
                        { timeUnit: "ytd", name: "YTD" },
                        { timeUnit: "year", count: 1, name: "1Y" },
                        { timeUnit: "year", count: 3, name: "3Y" },
                        { timeUnit: "max", name: "Max" }
                    ]
                }),
                seriesSwitcher,
                am5stock.DrawingControl.new(this.root, {
                    stockChart: this.stockChart
                }),
                am5stock.ResetControl.new(this.root, {
                    stockChart: this.stockChart
                }),
                am5stock.SettingsControl.new(this.root, {
                    stockChart: this.stockChart
                })
            ]
        })

        this.stockChart.children.push(
            am5.Picture.new(this.root, {
                width: 120,
                x: am5.percent(75),
                y: am5.percent(12),
                opacity: 0.4
            })
        );
    }

    //Show and hide bullets
    private showTransactions(enabled: boolean) {
        if (enabled && this.transactionSeries) {
            this.transactionSeries.show();
        }
        else if (!enabled && this.transactionSeries){
            this.transactionSeries.hide();
        }
    }

    // Function that dynamically loads data

    private loadData(ticker: any, series: any, granularity: any) {
        console.log('load data')
        let requestParams: any;
        fetch('../../../../assets/json/instrument-amazon-chart-data.json').then(res => res.json())
        .then((data:any) => {
          let result = data.map((res:any) => {
            console.log(res)
            if(res.adate){
              var splitdata=res.adate.split('T');
              var gettime= new Date(splitdata[0]).getTime();
              return {...res, adate:gettime};
            }
        })
                if (result) {
                    console.log('result',result)
                    // Process data (convert dates and values)
                    let processor = am5.DataProcessor.new(this.root, {
                        dateFields: ["adate"],
                        dateFormat: "yyyy-MM-dd",
                        numericFields: ["aopen", "ahigh", "alow", "aclose", "volume", "transaction_data.price_per_share"],
                    });
                    processor.processMany(result);
                    // Set data
                    am5.array.each(series, (item: any) => {
                        item.data.setAll(result);
                    });
                    if (ticker == this.SearchRecord[0]?.display_name) {
                        this.getSeriesColor("Volume");
                        this.seriesData = result;
                        this.toggleIndicator();
                    }
                }
         
            });
        this.getSeriesColor(ticker);
    }

    private getNewSettings(series: any) {
        let newSettings: any = [];
        am5.array.each(["name", "valueYField", "highValueYField", "lowValueYField", "openValueYField", "calculateAggregates", "valueXField", "xAxis", "yAxis", "legendValueText", "stroke", "fill"], (setting) => {
            newSettings[setting] = series.get(setting);
        });
        return newSettings;
    }

    private setSeriesType(seriesType: any) {
        // Get current series and its settings
        let currentSeries = this.stockChart.get("stockSeries");
        let newSettings = this.getNewSettings(currentSeries);
        this.resetValueSeriesSettings(seriesType, newSettings);
        // Remove previous series
        let data = currentSeries.data.values;
        this.mainPanel.series.removeValue(currentSeries);
        // Create new series
        let series!: { _settings: { name: any; stroke: { [x: string]: string | number; }; }; set: (arg0: string, arg1: am5.Color | am5.Tooltip) => void; fills: { template: { set: (arg0: string, arg1: any) => void; setAll: (arg0: { visible: boolean; fillOpacity: number; }) => void; }; }; strokes: { template: { setAll: (arg0: { strokeWidth: number; }) => void; }; }; columns: { template: { get: (arg0: string) => string[]; }; }; data: { setAll: (arg0: any) => void; }; };
        switch (seriesType) {
            case "line":
                series = this.mainPanel.series.push(am5xy.LineSeries.new(this.root, newSettings));
                if (series._settings.name == this.SearchRecord[0]?.display_name) {
                    this.targetSeries = series._settings.name;
                    this.getSeriesColor(this.targetSeries);
                    series.set("fill", am5.color(0xc6b54e));
                    series.set("stroke", am5.color(0xc6b54e));
                    series.fills.template.set(
                        "fillGradient",
                        am5.LinearGradient.new(this.root, {
                            stops: [
                                {
                                    opacity: 1,
                                    offsets: 0
                                },
                                {
                                    opacity: 0,
                                    offsets: 1
                                }
                            ],
                            rotation: 90
                        })
                    );

                    series.fills.template.setAll({
                        visible: true,
                        fillOpacity: 0.35
                    });
                    series.strokes.template.setAll({
                        strokeWidth: 2
                    });
                }
                break;
            case "candlestick":
            case "procandlestick":
                newSettings.clustered = false;
                series = this.mainPanel.series.push(am5xy.CandlestickSeries.new(this.root, newSettings));
                if (seriesType == "procandlestick") {
                    series.columns.template.get("themeTags").push("pro");
                }
                break;
            case "ohlc":
                newSettings.clustered = false;
                series = this.mainPanel.series.push(am5xy.OHLCSeries.new(this.root, newSettings));
                break;
        }
        // Set new series as stockSeries
        if (series) {
            this.valueLegend.data.removeValue(currentSeries);
            let tooltip:any = am5.Tooltip.new(this.root, {
                keepTargetHover: true,
                pointerOrientation: "left",
                labelText: "{valueY.formatNumber('#.00')}"
            });
            tooltip.get("background").setAll({
                stroke: am5.color(series._settings.stroke['hex']),
                strokeOpacity: 0.8
            });
            series.set("tooltip", tooltip);
            series.data.setAll(data);
            this.stockChart.set("stockSeries", series);
            let cursor = this.mainPanel.get("cursor");
            if (cursor) {
                cursor.set("snapToSeries", [series]);
            }
            this.valueLegend.data.insertIndex(0, series);
        }
    }

    private addComparingSeries(label: any) {
        if (label) {
            let series = am5xy.LineSeries.new(this.root, {
                name: label.data.factor,
                valueYField: label.data.factor,
                calculateAggregates: true,
                valueXField: "adate",
                xAxis: this.dateAxis,
                yAxis: this.valueAxis,
                legendValueText: "{valueY.formatNumber('#.00')}",
                tooltip: am5.Tooltip.new(this.root, {
                    keepTargetHover: true,
                    pointerOrientation: "left",
                    labelText: "{valueY.formatNumber('#.00')}"
                })
            });
            this.comparingSeries = this.stockChart.addComparingSeries(series);
            this.stockChart.set("compareSeries" + label.data.factor, this.comparingSeries);
            this.loadData(label.data.factor, [this.comparingSeries], this.currentGranularity);
            this.getSeriesColor(label.data.factor);
        }
    }

    private getSeriesColor(value: string) {
        let color: any; let rgb: any; let currentSeries: any;
        if (value.toLowerCase() == 'volume') {
            rgb = "rgb(87, 182, 92)";
        }
        else {
            let index = this.valueLegend._dataItems.findIndex((e: { dataContext: { _settings: { name: any; }; }; }) => e.dataContext._settings.name == value);
            if (index >= 0) {
                color = this.valueLegend._dataItems[index].dataContext._settings.stroke['hex'];
                rgb = this.decToRGB(parseInt(color));
            }
        }
        let seriesColor = {
            "series": value,
            "color": rgb
        }
        this.getSeriesColorCode.emit(seriesColor);
    }

    private decToRGB(dec: number) {
        const r = (dec & 0xff0000) >> 16;
        const g = (dec & 0x00ff00) >> 8;
        const b = (dec & 0x0000ff);
        return "rgb(" + r + ", " + g + ", " + b + ")";
    }

    //Modify value series settings for gradient
    private resetValueSeriesSettings(seriesType: any, settings: any) {
        if (seriesType == 'line') {
            settings.highValueYField = "";
            settings.lowValueYField = "";
            settings.openValueYField = "";
            settings.legendValueText = "Close: [bold]{valueY}[/]";
        }
        else {
            settings.highValueYField = 'ahigh';
            settings.lowValueYField = 'alow';
            settings.openValueYField = 'aopen';
            settings.legendValueText = "O: [bold]{openValueY}[/] H: [bold]{highValueY}[/] L: [bold]{lowValueY}[/] Close: [bold]{valueY}[/]";
        }
    }
    //hide loader
    private toggleIndicator() {
        if (this.indicator.isHidden()) {
            this.hourglassanimation.play();
            this.indicator.show();
        }
        else {
            this.hourglassanimation.pause();
            this.indicator.hide();
        }
    }

    ngOnDestroy() {
        if(this.root!=undefined)
        this.root.dispose();
    }
}