import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockChartGridComponent } from './stock-chart-grid.component';

describe('StockChartGridComponent', () => {
    let component: StockChartGridComponent;
    let fixture: ComponentFixture<StockChartGridComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [StockChartGridComponent]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(StockChartGridComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
