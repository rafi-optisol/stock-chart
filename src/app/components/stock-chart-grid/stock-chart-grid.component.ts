import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { CdkDropList, CdkDragEnter, CdkDropListGroup, CdkDragDrop, moveItemInArray, CdkDrag } from '@angular/cdk/drag-drop';
import { ViewportRuler } from '@angular/cdk/overlay';
import icSingle from '@iconify/icons-ic/twotone-square';
import icStacked from '@iconify/icons-ic/twotone-view-agenda';
import icGrid from '@iconify/icons-ic/twotone-grid-view';
import icDrag from '@iconify/icons-ic/twotone-open-with';
import icSearch from '@iconify/icons-ic/twotone-search';

@Component({
    selector: 'stock-chart-grid',
    templateUrl: './stock-chart-grid.html',
    styleUrls: ['./stock-chart-grid.scss']
})
export class StockChartGridComponent implements OnInit {

    @ViewChild(CdkDropListGroup) listGroup: CdkDropListGroup<CdkDropList>;
    @ViewChild(CdkDropList) placeholder: CdkDropList;
    @Input() icon = {
        "body": "<path fill=\"currentColor\" d=\"M15.5 14h-.79l-.28-.27A6.471 6.471 0 0 0 16 9.5A6.5 6.5 0 1 0 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5S14 7.01 14 9.5S11.99 14 9.5 14z\"/>",
        "width": 24,
        "height": 24
    };
    @Input() showInputPlaceholder = false;
    placeholderValue = 'Type Time Series';
    selectedLayout: "single"|"sides"|"stacked"|"grid" = "single";
    number: any = [];
    public getSearchValue : any = '';
    public target: any;
    public targetIndex: number;
    public source: any;
    public sourceIndex: number;
    public activeContainer:any;
    isSearch = {
        "body": "<path fill=\"currentColor\" d=\"M15.5 14h-.79l-.28-.27A6.471 6.471 0 0 0 16 9.5A6.5 6.5 0 1 0 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5S14 7.01 14 9.5S11.99 14 9.5 14z\"/>",
        "width": 24,
        "height": 24
    };
    icSingle = {
        "body": "<path fill=\"currentColor\" d=\"M5 5h14v14H5z\" opacity=\".3\"/><path fill=\"currentColor\" d=\"M3 3v18h18V3H3zm16 16H5V5h14v14z\"/>",
        "width": 24,
        "height": 24
    };
    icStacked = {
        "body": "<path fill=\"currentColor\" d=\"M5 5h14v4H5zm0 10h14v4H5z\" opacity=\".3\"/><path fill=\"currentColor\" d=\"M19 13H5c-1.1 0-2 .9-2 2v4c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2v-4c0-1.1-.9-2-2-2zm0 6H5v-4h14v4zm0-16H5c-1.1 0-2 .9-2 2v4c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm0 6H5V5h14v4z\"/>",
        "width": 24,
        "height": 24
    };
    icGrid = {
        "body": "<path fill=\"currentColor\" d=\"M5 5h4v4H5zm0 10h4v4H5zm10 0h4v4h-4zm0-10h4v4h-4z\" opacity=\".3\"/><path fill=\"currentColor\" d=\"M3 21h8v-8H3v8zm2-6h4v4H5v-4zm-2-4h8V3H3v8zm2-6h4v4H5V5zm8 16h8v-8h-8v8zm2-6h4v4h-4v-4zM13 3v8h8V3h-8zm6 6h-4V5h4v4z\"/>",
        "width": 24,
        "height": 24
    };
    icDrag = {
        "body": "<path fill=\"currentColor\" d=\"M10 9h4V6h3l-5-5l-5 5h3v3zm-1 1H6V7l-5 5l5 5v-3h3v-4zm14 2l-5-5v3h-3v4h3v3l5-5zm-9 3h-4v3H7l5 5l5-5h-3v-3z\"/>",
        "width": 24,
        "height": 24
    };
    search: any;

    constructor(private viewportRuler: ViewportRuler) {
        this.target = null;
        this.source = null;
    }

    ngOnInit(): void {
        console.log(this.icon)
        this.number.push(1);
    }
   

    selectLayout(value: "single"|"sides"|"stacked"|"grid") {
        this.number = [];
        if (value == 'single')
            this.number.push(1);
        if (value == 'stacked' || value == 'sides') {
            this.number.push(1);
            this.number.push(2);
        }
        if (value == 'grid') {
            this.number.push(1);
            this.number.push(2);
            this.number.push(3);
            this.number.push(4);
        }
        this.search = this.getSearchValue;
    }

    
    searchValue(eve:any) {
        if(/^[a-zA-Z0-9_.+-]*(?:[a-zA-Z][a-zA-Z0-9_.+-]*){2,}$/.test(this.getSearchValue)) {
                this.search = this.getSearchValue;
        }
    }
}