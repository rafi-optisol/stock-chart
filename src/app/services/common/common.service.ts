import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs';
import { environment } from '../../../environments/environment'
@Injectable({
  providedIn: 'root'
})
export class CommonService {

  public Baseurl = environment.Apiurl

  constructor(private http: HttpClient) { }

  public getApi(Apiurl: any): Observable<any> {
    return this.http.get(this.Baseurl + Apiurl)
  }
  public postApi(Apiurl: any, payload: any): Observable<any> {
    return this.http.post(this.Baseurl + Apiurl, payload);
  }
  public putApi(Apiurl: any, payload: any): Observable<any> {
    return this.http.post(this.Baseurl + Apiurl, payload);
  }
  public deleteApi(Apiurl: any, payload: any): Observable<any> {
    return this.http.delete(Apiurl + payload);
  }

}
