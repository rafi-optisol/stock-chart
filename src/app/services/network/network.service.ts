import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      // 'Access-Control-Allow-Origin': '*',
      // 'Authorization': localStorage.getItem(AppLocalStorageKeys.APP_Token)
    })
  };
  private Environment = environment.apiBaseUrl;

  constructor(private http: HttpClient) {
  }

  getHttpClientRequest(Url: string): Observable<any> {
    const APIUrl = this.Environment + Url;
    return this.http.get(APIUrl);
  }

  postHttpClientRequest(Url: string, data: any, option?: any): Observable<any> {
    const APIUrl = this.Environment + Url;
    return this.http.post(APIUrl, data, this.httpOptions);
  }

  putHttpClientRequest(Url: string, data: any, option?: any): Observable<any> {
    const APIUrl = this.Environment + Url;
    return this.http.put(APIUrl, data, this.httpOptions);
  }

  patchHttpClientRequest(Url: string, data: any, option?: any): Observable<any> {
    const APIUrl = this.Environment + Url;
    return this.http.patch(APIUrl, data, this.httpOptions);
  }

  deleteHttpClientRequest(Url: string, option: any): Observable<any> {
    const APIUrl = this.Environment + Url;
    return this.http.delete(APIUrl, this.httpOptions);
  }
}
