export const Languages = {
    "ALLOWED_LIST": ['en', 'fr'],
    "ALLOWED_LIST_MATCH": /en|fr/,
    "DEFAULT": 'en',
    "LANG_OPTIONS": {
        "English": "en",   
        "French": "fr",
    }
};

export const AppLocalStorageKeys = {
    APP_Token: 'appToken',
    AUTH_Token: 'authToken',
    Lang: 'lang'
  };


  export const CryptoJSKeys = {
    uniqueKey: ''
  };